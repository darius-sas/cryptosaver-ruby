
require 'openssl'
require_relative './lib/pbkdf2'

class Crypter
	def initialize password
		@enc_cipher = OpenSSL::Cipher.new 'aes-256-cbc'
		@enc_cipher.encrypt # encrypt mode
		@key = create_key password, 32 # create key
		@iv = create_key password, 16  # create key

		@enc_cipher.key = @key
		@enc_cipher.iv = @iv

		@dec_cipher = OpenSSL::Cipher.new 'aes-256-cbc'
		@dec_cipher.decrypt # decrypt mode
		@dec_cipher.key = @key
		@dec_cipher.iv = @iv

		@input_stream = nil
		@output_stream = nil
	end

	def encrypt_string str
		enc_str = @enc_cipher.update str
		enc_str << @enc_cipher.final
	end

	def decrypt_string str
		dec_str = @dec_cipher.update str
		dec_str << @dec_cipher.final
	end

	def encrypt_stream inp, outp
		if !inp.closed?
			@input_stream = inp
			@output_stream = outp
			buffer = ''
			i = 0
			inp.each_char do |c|
				buffer += c
			end
			outp.write encrypt_string buffer
		end
	end

	def decrypt_stream inp, outp
		if !inp.closed?
			@input_stream = inp
			@output_stream = outp
			buffer = ''
			i = 0
			inp.each do |c|
				buffer += c
			end
			outp.write decrypt_string buffer
		end
	end

	def close_streams
		@input_stream.close  if @input_stream != nil
		@output_stream.close if @output_stream != nil
	end

	private
	def create_key password, length
		salt_a = [0x01, 0x02, 0x23, 0x34, 0x37, 0x48, 0x24, 0x63, 0x99, 0x04]
		salt = salt_a.pack('C*')
		iterations = 1000
		derived_b = PBKDF2.new do |p| 
		  p.password = password
		  p.salt = salt
		  p.iterations = iterations
		  p.key_length = length
		  p.hash_function = OpenSSL::Digest::SHA1
		end
		# derived_b.bin_string
		OpenSSL::PKCS5.pbkdf2_hmac_sha1 password, salt, iterations, length
	end
end