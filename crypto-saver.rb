#!/bin/ruby

require_relative 'crypter'
require_relative 'models'

require 'thor'
require 'clipboard'

class CryptoSaver < Thor

	PASSWORD_FILE_NAME = 'pass_xml.csp' # CHANGE FILE NAME

	no_commands do
		if !File.exist? PASSWORD_FILE_NAME
			File.open(PASSWORD_FILE_NAME, "w").close
		end

		def get_passwd
			print "Please insert master password: "
			s = STDIN.noecho(&:gets)
			puts ""
			s.chomp
		end

		def get_xml file, password
			xml = StringIO.new
			crp = Crypter.new password
			crp.decrypt_stream file, xml
			crp.close_streams
			xml
		end

		def set_xml xml, file, password
			crp = Crypter.new password
			crp.encrypt_stream StringIO.new(xml), file
			crp.close_streams
		end

		def get_accounts_from_file
			pswd = get_passwd
			if File.zero? PASSWORD_FILE_NAME
				accounts = Accounts.new
			else
				xml = get_xml File.open(PASSWORD_FILE_NAME), pswd
				accounts = Ox.parse_obj xml.string
			end
			yield accounts
			xml = Ox.dump accounts
			set_xml xml, File.open(PASSWORD_FILE_NAME, "w+"), pswd
		end
	end

	desc "get acc-name", "Retrieve account data saved."
	option :clipboard, :aliases => "-c", :type => :boolean, 
		:default => true, :desc => "Copy account password to clipboard."
	def get account_name
		if File.zero? PASSWORD_FILE_NAME
			puts "No accounts were saved."
			exit
		end
		begin
			account = nil
			get_accounts_from_file do |accounts|			
				account = accounts.find_by_name account_name
			end
			if account.nil?
				puts "Account not found."
			else
				puts account

				if options[:clipboard]
					Clipboard.copy account.password # Copy to clipboard
					puts "Password was copied to clipboard."
				end
			end
		rescue OpenSSL::Cipher::CipherError
			puts "Decryption failed: password is wrong."
		end
	end

	desc "set", "Save an account."
	option :name, :aliases => "-n", :required => true, :desc => "Name."
	option :username, :aliases => "-u", :desc => "Username."
	option :password, :aliases => "-p", :desc => "Password."
	option :email, :aliases => "-e", :desc => "Email."
	option :description, :aliases => "-d", :desc => "Description."
	def set
		get_accounts_from_file do |accounts|
			new_account = Account.new do |a|
				a.name = options[:name]
				a.username = options[:username]
				a.password = options[:password]
				a.email = options[:email]
				a.description = options[:description]
			end
			accounts.add new_account
		end
		puts "Account saved."
	end

	desc "del acc-name", "Delete an account."
	def del account_name
		get_accounts_from_file do |accounts|
			deleting_account = accounts.find_by_name account_name
			print "Are you sure you want to delete #{account_name}? (y/n) "
			loop {
				answer = STDIN.gets.chomp
				if !answer.nil? && !answer.empty? && answer[0].downcase == 'y'
					accounts.remove deleting_account
					puts "Account has been deleted."
					break
				elsif !answer.nil? && !answer.empty? && answer[0].downcase == 'n'
					puts "Account has NOT been deleted."
					break
				else
					print "Please, aswner \"n\" or \"y\": "
				end
			}
			exit
		end
	end

	desc "mod", "Edit an account."
	option :name, :aliases => "-n", :required => true, :desc => "Name."
	option :new_name, :aliases => "-a", :desc => "New account name."
	option :username, :aliases => "-u", :desc => "New username."
	option :password, :aliases => "-p", :desc => "New password."
	option :email, :aliases => "-e", :desc => "New email."
	option :description, :aliases => "-d", :desc => "New description."
	def mod old_name
		get_accounts_from_file do |accounts|
			mod_account = accounts.find_by_name old_name
			if mod_account.nil?
				puts "Account not found."				
			elsif accounts.find_by_name(options[:new_name]).nil?
				puts "Account name already exists, try another one."
			else
				accounts.remove mod_account
				mod_account.name = options[:new_name]
				mod_account.username = options[:username]
				mod_account.password = options[:password]
				mod_account.email = options[:email]
				mod_account.description = options[:description]
				accounts.add mod_account

				puts "Account successfully modified."
			end
		end
	end

	desc "list", "Lists accounts name only."
	def list
		get_accounts_from_file do |accounts|
			accounts.each_with_index do |a, i|
				puts "#{i+1}: #{a.name}"
			end
		end
	end
end

CryptoSaver.start ARGV