
require 'ox'

class Account
	attr_accessor :name, :username, :password, :email, :description

	def initialize
		yield self
	end

	def to_s
		s = "\tName: #{@name}\n\tUsername: #{@username}\n\tPassword: ******* \n\t"	
		s += "Email: #{@email}\n\tDescription: #{@description}"
	end
end

class Accounts

	def initialize
		@acc_list = Array.new
	end

	def add *accs
		accs.flatten.each {|x| @acc_list.push x}		
	end

	def remove a
		@acc_list.delete a
	end

	def size
		@acc_list.length
	end

	def find_by_name a
		account = nil
		@acc_list.each { |x| account = x if x.name == a}
		account
	end

	def length
		self.size
	end

	def each
		@acc_list.each { |x| yield x}
	end

	def each_with_index
		i = 0;
		@acc_list.each { |x| yield x, i; i += 1 }
	end

	def to_s
		s = ''
		s += "Size: #{size}\nAccounts:\n" 
		@acc_list.each { |x| s += x.to_s + "\n\n" }
		s
	end
end